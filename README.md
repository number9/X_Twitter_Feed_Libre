# X_Twitter_Feed_Libre

Gitlab Repository: https://gitlab.com/libre-tech/X_Twitter_Feed_Libre.git/

Public JSON Data of Twitter Feed:  https://libre-tech.gitlab.io/X_Twitter_Feed_Libre/index.json"

# Add Tweets

Want to add Tweets to this list for a BRC20?

Fork this repo and submit a PR with a an array of Tweet(X) IDs added to append to the index.json here in this repo.

Example:

Here is a tweet - let's say I want to make this show for the token "BRC1":
https://twitter.com/bensig/status/1687146117396406284


This can be done by typing:

`python add_tweet BRC1 https://twitter.com/bensig/status/1687146117396406284`

or 

`python add_tweet BRC1 1687146117396406284`

Here is the data that will be added to the bottom of index.json:

```
  "BRC1": [
    "1687146117396406284"
  ]
```

We will review and merge these PRs. Thanks!